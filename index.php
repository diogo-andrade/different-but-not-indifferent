﻿<!DOCTYPE html>
<head>
<title>Different But Not Indifferent</title>

<script src="js/jquery.js"></script>
<script src="js/core.js"></script>
<script src="js/ga.js" async="" type="text/javascript"></script>
<script src="js/bsa.js"></script>
<script src="js/styleswitch.js"></script>
<script src="js/goalProgress.min.js"></script>

<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/style6.css">
<link href="css/css.css" rel="stylesheet" type="text/css">
<link href="css/core.css" rel="stylesheet" type="text/css">
<link href="css/normalize.css" rel="stylesheet" type="text/css">
<link href="css/goalProgress.css" rel="stylesheet" type="text/css">
<link href="css/pt.css" rel="stylesheet" type="text/css">
<link href="css/en.css" rel="alternate stylesheet" type="text/css" title="alternate">


<script>
var language = window.navigator.userLanguage || window.navigator.language;
var cookieLang = getCookie("mysheet");

$(document).ready(function(){
        $('#sample_goal').goalProgress({
            goalAmount: 1000,
            currentAmount: 484,
            textBefore: '',
            textAfter: '€ angariado.'
        });
    });

if(cookieLang=="none" || cookieLang =="alternative") {
	setStylesheet(cookieLang);
} else if(language == "pt-PT" || language == "pt-BR" || language == "pt") {
	changeLang('none',60);

} else {
	changeLang('alternate',60);
}


$(function() {
	var pull 		= $('#pull');
		menu 		= $('nav ul');
		menuHeight	= menu.height();

	$(pull).on('click', function(e) {
		e.preventDefault();
		menu.slideToggle();
	});

	$(window).resize(function() {
		var w = $(window).width();
		if(w > 320 && menu.is(':hidden')) {
			menu.removeAttr('style');
		}
	});
});



</script>
</head>

<body>
<nav class="clearfix">
	<ul class="clearfix">
		<li><a class="Sobre-o-Projecto" href="#Sobre-o-Projecto"><span class="pt">O Projecto</span><span class="en">The Project</span></a></li>
		<li><a class="Sobre-Nos" href="#Sobre-Nos"><span class="pt">Sobre Nós</span><span class="en">About Us</span></a></li>
		<li><a class="Doar" href="#Doar"><span class="pt">Doar</span><span class="en">Donate</span></a></li>
		<li><a class="Contactar" href="#Contactar"><span class="pt">Contactar</span><span class="en">Contact</span></a></li>

		<li><a href="javascript:changeLang('none',%2060)" style="background:none;border:none; width:40px; margin-left:50px;"><img style="width:40px; height: auto;" src="images/pt.png"/></a></li>
		<li><a href="javascript:changeLang('alternate',%2060)" style="background:none;width:40px;margin-left:30px;"><img style="width:40px; height: auto;" src="images/en.png"/></a></li>
	</ul>
	<a href="#" id="pull">Menu</a>
</nav>


<div class="wrapper">
<section class="container">
<div id="header">
<img src="images/header.png"/>
</div>

<div id="sample_goal"></div>

<div id="button">
<span class="pt">
<a href="#Doar" class="button big gray">Doar <span>agora!</span></a>
</span>

<span class="en">
<a href="#Doar" class="button big gray">Donate <span>now!</span></a>
</span>
</div>


<img src="images/image.png"/>



<hr id="Sobre-o-Projecto">

<article id="article-1">
<h1 class="pt">Sobre o Projecto</h1>
<h1 class="en">About the Project</h1>

<span class="pt">No Tarrafal existem mais de 100 crianças/adolescentes com necessidades especiais, 
na sua maioria com paralisia cerebral. Estas crianças Diferentes são, muitas vezes, 
tratadas com Indiferença, pela sociedade que os rodeia, pelo governo e por vezes pelos seus familiares. 
Na maioria dos casos o pai abandona a família pelo seu filho ser "diferente". Algumas mães precisam de 
deixar os filhos sozinhos em casa para poderem trabalhar, outras, ainda, sentem vergonha, e outras acham 
que os seus filhos não tem necessidades como nós e que podem, simplesmente, ficar sozinhos em casa. 
Mas talvez seja melhor contarmos-vos o porquê deste projecto ter surgido.</span>

<span class="en">There are more than 100 children/teenagers in Tarrafal who have special needs: most of them with cerebral paralysis. These Different children are many times treated with Indifference, by the society that surrounds them, by the government and some times even by their relatives. In most of the cases the father abandons the family because the child is “different”. Some mothers need to leave their children home alone so they can work, others feel ashamed, and some think their child does not have any special needs, and they can simply leave them at home. But perhaps we should tell how and why this project has emerged.</span>
</article>
<blockquote class="pt">
"No ano passado parti pela primeira vez em missão internacional de curta duração. Quando fomos 
fazer questionários pelo Tarrafal tínhamos uma pergunta fundamental: "Qual a sua maior preocupação?". 
Em Chão-Bom, uma senhora respondeu-me: "Sou de um grupo de Batuco e queremos gravar um CD e não conseguimos". 
Fiquei perplexa com tal resposta, afinal, estávamos num sítio que não tem água nem electricidade, e o 
problema é não poderem gravar um CD. Puxamor por ela, e então, depois de falar da água, da luz, dos 
baixos salários, lá falou no seu filho. Contou que ele ficava sozinho todos os dias em casa e levou-nos 
a conhecê-lo. Um rapaz com um sorriso lindo e bastante sociável. A mãe dizia que não valia a pena ele ir 
para a escola, pois, não iria aprender a escrever de qualquer maneira. Fomos embora com a certeza que 
teríamos de voltar a falar com esta senhora. Quando voltámos estava o Danic fechado em casa sozinho, e 
depois lá apareceu a sua mãe. Com a ajuda de uma local, conseguimos convencê-la a levá-lo a delegação de 
educação. A psicóloga de lá, já nos tinha dito que podia receber estas crianças em algumas salas. Mas 
o Danic não teve sorte, e a Delegação de educação declarou que ele não poderia frequentar uma sala com 
crianças "normais" devido ao seu estado avançado. Essa notícia chegou quando eu já estava em Portugal, 
desde aí, que me lembro do Danic com uma revolta e uma tristeza enorme. Ele é diferente mas não é 
indiferente. Conheci também a Silvina, que vive com os avós, tios e primos, no entanto, ela passa a 
maior parte do seu dia sozinha sem ninguém para brincar. Mas, sempre vai tendo algum movimento em casa. 
Estas foram as crianças que mais me marcaram, pela solidão que passam em grande parte do seu dia." <p style="text-align:right;"> Bárbara
</blockquote>

<blockquote class="en">
"Last year I participated for the first time on an international short duration mission. When we went on with a survey in Tarrafal, we had one fundamental question: “What is your biggest preoccupation?”. In Chao-Bom, a lady answered: “I am from a group of Batuco and we want to record a CD but we can’t manage to do that.” I was amazed by her answer, since we were in a place with no water or electricity, and the problem was not being able to record a CD. We wanted to know more, so after talking about water, electricity, low salary, she started talking about her son. She told us that she had to be home with her son every day, and she took us to meet him: a boy with a beautiful smile and quite sociable. His mother was telling us that he is not worthy of going to school, because he wouldn’t learn to read and write anyway. We left the place with one thing in mind: that we must go back and talk to this lady again. When we returned, we found Danic home alone. After a while, his mother returned. With the help of a local, we managed to convince her to allow him to go to a delegation of education. The psychologist confirmed that she could take these children in some of the rooms. Danic was not as lucky though, as the delegation declared that he could not attend a class with “normal” children because of his advanced state. These news arrived while I was back in Portugal. Since then I remind myself of Danic with great sadness. He is different, but not indifferent. I also met Silvina, who lives with her grandparents, uncles and cousins. However, she is spending her time mostly alone, with nobody to play with. These were the children that marked me by the way they spend most of the day in solitude." <p style="text-align:right;"> Bárbara
</blockquote>
<article style="margin-top:-3px;">
<span class="pt">
Por eles, e por todos os Danics e Silvinas do Tarrafal, pretendemos ir este ano montar um projecto 
que garanta a alegria destas crianças. Queremos que eles aprendam algo, que tenham amigos, que tenham
 quem lhes dê água e comida durante o dia, quem lhes mude a fralda ou os leve a casa de banho, quem 
 os faça rir todos os dias. Pedimos-te ajuda!
	<p>A vontade é muita mas, o dinheiro nem tanto. Precisamos de
 ti para que estas crianças diferentes não tenham uma vida indiferente.</p>
</span>

<span class="en">
For them and for every other Danics and Silvinas from Tarrafal, we aim to go back there and accomplish a project that guarantees the happiness of these children. We want them to learn some things, we want them to have friends, and we want them to have somebody that feeds them, and gives them water everyday, somebody to change the diapers or take them to the toilet. Somebody to makes them smile every day. We ask for your help!
	<p>The will is strong, but we lack of money. We need your help to make a difference.</p>
</span>

</article>
<hr id="Sobre-Nos">

<section>
<h1 class="pt">Sobre Nós</h1>
<h1 class="en">About Us</h1>
<span class="pt">
<p>O projecto é constituído por dois voluntários portugueses. Contam com o apoio dos 
Escuteiros do Tarrafal, e estão em contacto com a Delegação de educação.</p>
<p>Bárbara Água - Esteve em missão de curto prazo no Tarrafal pela associação GASTagus. O trabalho da 
missão anterior foi levar novos conhecimentos em diversas áreas para contribuir com o desenvolvimento 
do Tarrafal, trabalhando assim, com várias organizações e grupos do Tarrafal. Já fez voluntáriado com 
crianças e adultos com paralisia cerebral em Hipoterapia durante quatro anos. É estudante de artes 
do espectáculo, trabalha como promotora para várias agências e tem 25 anos.</p>

<p>Diogo Andrade - Esteve 
em missão de curto prazo também pelo GASTagus, em Mocumbi, no interior de Moçambique. O trabalho foi 
realizado com a comunidade lá presente, professores e alunos. Com crianças e adolescentes foram 
desenvolvidos trabalhos na área da educação, e com os professores na área da informática e da pedagogia. 
É licenciado em Eng. Informática, e tem 22 anos.</p>
</span>

<span class="en">
<p>The team is built by two volunteer Portuguese members. We have the support of the Scouts of Tarrafal, and we are in contact with the Educational delegation.</p>
<p>Bárbara Água – Was on a short-term mission in Tarrafal through the GASTagus association. The main goal of the previous mission was to convince these diverse areas to contribute with the development of Tarrafal, working like this with various local organizations and groups. She also volunteered during four years in Hipotherapy with children and adults with cerebral paralysis. She is 25 years old, studies arts and theater, and works as a prosecutor for various agencies.</p> 

<p>Diogo Andrade – Was on a short-term mission, also through GASTagus, in Mocumbi, in the middle of Mozambique. The work was accomplished with the local community, teachers and students. He helped develop children and adults in the area of education, and with teachers, in the area of informatics and pedagogy. He is 22 years old and licensed in computer engineering.</p>
</span>
</section>

<hr id="Doar">

<section>
<h1 class="pt">Doar</h1>
<h1 class="en">Donate</h1>

<span class="pt">
<p>Estamos na primeira fase do projecto (Estruturação), nesta fase precisamos de
angariar 1000€ para que possamos nos deslocar até ao Tarrafal e assim fazer todos os
contactos que não são possíveis atrás de um computador ou de um telefone.</p>

<p>Faça o seu donativo, por transferência bancária, para o IBAN:</p>
</span>

<span class="en">
<p>We are in the first phase of the project (Planning), and in this phase we need to collect 1000€ so we could travel to Tarrafal, and there achieve all the contacts that are not possible through a computer or phone.</p>

<p>Please donate by bank transfer to the following IBAN:</p>
</span>
<div id="iban">
<h3>Banco Santander Totta</h3>
<h3>PT50 0018 0003 36353480020 86</h3>
<h3>BIC/SWIFT: TOTAPTPL</h3>
<h3>Bárbara Água Ferreira Pereira</h3>
</div>


<span class="pt">
<p>Recompensa</p>

<p>Doe 2 € - Um agradecimento especial e particular no facebook.</p>	
<p>Doe 5 € - Postal digital com as crianças do nosso projecto.</p>
<p>Doe 10 € - Envio das fotografias tiradas em missão e uma fotografia com agradecimento personalizado.</p>	
<p>Doe 20 € - Envio das fotografias tiradas em missão + uma fotografia com agradecimento personalizado + um video com as filmagens feitas em missão.</p>
<p>Doe 50 € - Envio das fotografias tiradas em missão + uma fotografia com agradecimento personalizado + um video com as filmagens feitas em missão + um jantar em nossa casa para contarmos toda a missão.</p>
<br>
<p>Para seres recompensado, envia-nos o comprovativo da tua transferência para o e-mail:  barbara.agua.pereira@gmail.com</p>
</span>

<span class="en">
<p>Prizes:</p>

<p>Donate 2 € - A special thank on Facebook.</p>	
<p>Donate 5 € - An e-card with the children of our project.</p>
<p>Donate 10 € - You receive the photographies made on the mission and a photography with personalized thanking.</p>	
<p>Donate 20 € - You receive the photographies made on the mission, a photography with personalized thanking and a video of the mission.</p>
<p>Donate 50 € - You receive the photographies made on the mission, a photography with personalized thanking and a video of the mission. And a meal at our house to report you about our mission.</p>
<br>
<p>In order to receive the prize, send us the paper of the transference to the following e-mail:		                     barbara.agua.pereira@gmail.com</p>
</span>



</section>

<hr id="Contactar">

<section>
<h1 class="pt">Contactar</h1>
<h1 class="en">Contact</h1>


<p class="pt">Quer saber mais sobre nós ou sobre o projecto? Contacte-nos!</p>
<p class="en">Do you want to know more about us or the project? Contact us!</p>

<form name="contactform" method="post" action="send_form_email.php">
 
<table width="450px">
<tr>
 <td valign="top">
  <label class="pt" for="first_name">Nome *</label>
  <label class="en" for="first_name">First Name *</label>
 </td>
 <td valign="top">
  <input  type="text" name="first_name" maxlength="50" size="30">
 </td>
</tr>
<tr>
 <td valign="top">
  <label class="pt" for="last_name">Apelido *</label>
  <label class="en" for="last_name">Last Name *</label>
 </td>
 <td valign="top">
  <input  type="text" name="last_name" maxlength="50" size="30">
 </td>
</tr>
<tr>
 <td valign="top">
  <label for="email">Email *</label>
 </td>
 <td valign="top">
  <input  type="text" name="email" maxlength="80" size="30">
 </td>
</tr>
<tr>
 <td valign="top">
  <label class="pt" for="comments">Mensagem *</label>
  <label class="en" for="comments">Message *</label>
 </td>
 <td valign="top">
  <textarea  name="comments" maxlength="1000" cols="25" rows="6"></textarea>
 </td>
</tr>
<tr>
 <td colspan="2" style="text-align:left">
  <input class="pt" type="submit" value="Enviar">
  <input class="en" type="submit" value="Send">
 </td>
</tr>
</table>
</form>
</section>


<a style="display: inline;" href="#" class="scrollToTop">
  <span class="pt">Voltar ao Topo</span>
  <span class="en">Back to Top</span>
</a>

</div>
</section>
<div id="clear"></div>
</body>
</html>
